# Load the Pandas libraries with alias 'pd'
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def euclidean():
    pass

def main():
    dataset_file = "dataset.csv"

    ### We have 13 attributes for each dataset row
    ### read the datset file in the pandas dataFrame

    data = pd.read_csv(dataset_file, delimiter = ",", encoding = "utf-8",
                        names = ['attr1','attr2','attr3',
                        'attr4','attr5','attr6','attr7',
                        'attr8','attr9','attr10','attr11','attr12','attr13'])
    data_mean = pd.Series(data.mean())
    data_mean = data_mean.to_dict()

    print("MEAN VALUES")
    for key in data_mean:
        print(f'{key} ---> {data_mean[key]}')


    ## Scater VALUES

    data.plot.scatter(x='attr1', y='attr2', figsize=(8,6))



if __name__ == "__main__":
    main()
